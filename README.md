# Introduction

This is a [Serverless Framework](https://github.com/serverless/serverless) project which will automatically start and stop EC2 instances.

It creates two scheduled events. One will start a list of EC2 instances and the other will stop a list of EC2 instances. The list of instances is defined in the [AWS Systems Manager Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html). The project also sets up the necessary IAM policies. No manual AWS configuration is required!

## Install

These instructions are for Linux (specifically Ubuntu). For other environments, a quick web search will help!

### Install Serverless

```
# Install node and npm
sudo apt update
sudo apt install nodejs
sudo apt install npm

# Install the serverless cli
npm install -g serverless

# Create and deploy a new service/project
serverless
```

### Install AWS CLI

```
# Install
sudo apt-get install awscli

# Configure
aws configure
```

### Install EC2 Start/Stop

```
# Install git
sudo apt install git

# Clone the repo
git clone git@gitlab.com:raytio-public/tools/ec2_start_stop.git
cd ec2_start_stop
```

## Set up

### Decide which instances you want to start and stop

To get a list of instance ids:

```
aws ec2 describe-instances --output json
```

Note the values of the `InstanceId` fields in the returned response

### Create the relevant parameters in SSM

Create two parameters with the `Type` of `String`. The name of the parameters will be as follows:

```

/{stage}/aws_region
/{stage}/ec2_instances
```

The `stage` is a [Serverless concept](https://serverless-stack.com/chapters/stages-in-serverless-framework.html) that allows deployments to be grouped. In the following examples we use a `stage` named `dev`. To create a schedule for a separate set of instances just use the name of that `stage` e.g. `staging`

Set the value of each to be the relevant region and the list of instances to start and stop. For example to have the script start and stop 2 `dev` instances in the `us-east-1` region:

```
aws ssm put-parameter --name /dev/aws_region --type String --value us-east-1 --output json
aws ssm put-parameter --name /dev/ec2_instances --type String --value ['i-0c7cdb180150fabc', 'i-0c7c3760bd2fdc4e9'] --output json
```

### Tag the EC2 instance

Against each instance type, create a `Type` tag with the value of the stage. This tag is checked by the IAM policy to ensure that the event has permissions for the instances in the list. The tag should correspond to the `stage` name. For example, for your development EC2 instances, set `Type` to `dev`.

```
aws ec2 create-tags --resources i-0c7cdb180150fabc i-0c7c3760bd2fdc4e9 --tags Key=Type,Value=dev --output json
```

### Set up the schedule

Update the `schedule` for the `ec2_start` and `ec2_stop` functions in the `serverless.yml` file. Use either the AWS cron or rate syntax. An example which will run the function every day at 1700:

```
cron(0 19 * * ? *)
```

## Deploy

To deploy to the `dev` stage

```
sls deploy --stage dev
```
